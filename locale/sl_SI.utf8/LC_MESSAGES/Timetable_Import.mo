��    /      �  C                    :     B     N  Y   h  
   �     �     �     �     �        *        /     6     =     J     X     a     r     �     �     �     �     �     �  %   �     �          #  
   (     3  
   :     E     J     S     Y     r     w     �     �     �     �     �     �     �  	   �  �  �  %   �  	   �     �  !    	  h   "	  
   �	     �	     �	     �	  !   �	     �	  1   �	     
     #
     )
     0
     7
     F
     S
     f
     m
  %   �
  
   �
     �
     �
  *   �
          -     M     S     _     g  	   y     �     �     �     �     �     �     �  
   �     �     �                    .          !                                 	                                                             #         )             ,                         $   "   *       &                 +   -   %      /   (           
   '          %s Course Periods were imported. %s rows Activity ID Allow Teacher Grade Scale Are you absolutely ready to import timetable? Make sure you have backed up your database! Attendance Calendar Cannot open file. Credits Database Backup Day Delete existing Courses and Course Periods Female Friday Grade Levels Grading Scale Half Day Import Timetable Import first row Male Marking Period Maximum file size: %01.0fMb Monday No day found. No period found. No student sets / grade levels found. No subject found. No teacher found. None Not Graded Period Reset form Room Saturday Seats Select CSV or Excel file Stop Student Sets Subject Sunday Teacher Thursday Timetable Fields Timetable Import Tuesday Wednesday Project-Id-Version: Timetable Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-16 11:38+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.3.2
X-Poedit-SearchPath-0: .
 Uvoženih je bilo %s vzgojnih skupin. %s vrstic ID dejavnosti Dovoli lestvico ocen vzgojiteljev Ali ste popolnoma pripravljeni na uvoz urnika? Prepričajte se, da ste varnostno kopirali bazo podatkov! Prisotnost Koledar Datoteke ni mogoče odpreti. Krediti Varnostno kopiranje baze podatkov Dan Izbrišite obstoječe tečaje in obdobja tečajev Ženski Petek Letnik Letnik Polovica Dneva Uvozni urnik Uvozi prvo vrstico Moški Obdobje označevanja Največja velikost datoteke: %01.0fMb Ponedeljek Noben dan ni bil najden. Nobena perioda ni bila najdena. Noben dijakov sedež/letnik ni bil najden. Tema ni bila najdena. Noben vzgojitelj ni bil najden. Noben Ni ocnejeno Perioda Ponastavi obrazec Učilnica Sobota Sedeži Izberite datoteko CSV ali Excel Ustavi Dijakov sedež Predmet Nedelja Vzgojitelj Četrtek Polja vzgojnih skupin Uvoz urnika Torek Sreda 